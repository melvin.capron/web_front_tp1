import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Entry from "@/components/Entry";
import GameList from "@/components/GameList";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/list',
        name: 'Liste de jeux',
        component: GameList
    },
    {
        path: '/simon',
        name: 'Simon',
        component: Entry
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
